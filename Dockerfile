FROM alpine:3.6
LABEL maintainer="Fabian Laule - mail@fabianlaule.com"

RUN apk add --no-cache ca-certificates

COPY prometheus_bot /

EXPOSE 9087
ENTRYPOINT ["/prometheus_bot"]
CMD ["-c=/etc/prometheus_bot/config.yml"]